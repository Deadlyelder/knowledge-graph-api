# Load Graph and Semantic Rules file Upload 

## Description
This is the API that is used for the creation of the Knowledge Graph into Neo4j and the uploading of the file 
containing the Semantic Rules.

It provides two endpoints, one for uploading the .json file exported from SAURON Propagation Engine Editor 
(https://atlas.ait.ac.at/sauron/#/) and another for uploading the .ttl file that contains the Semantic Rules 
for the data.


## Usage
After cloning the project on your local machine/server, type:
- cd knowledge-graph-api
- docker-compose up --build

For uploading the .json file from SAURON to create the knowledge graph into neo4j, open an internet browser and go to
the url: http://localhost:3061/upload_graph_file. The following page will show up, where you can choose the file to 
upload (visual 1). 

Visual 1: Screen for uploading the .json file exported from SAURON
![](pics/upload_sauron_file1.png "upload_sauron")

After choosing the .json file, click on "Create Graph in Neo4j" and upon successful completion, the following messages will 
appear:
- File successfully Uploaded
- Previous Graph Deleted
- New Graph Created

To check if the graph is created in Neo4j, go to the url: http://localhost:3060/browser/ and login with credentials: 
Username: neo4j / Password: neo4j_user . Then execute the query "MATCH (N) RETURN N", on the command line. The graph 
will appear as shown in visual 2.

Visual 2: Knowledge Graph in Neo4j after the upload of SAURON file
![](pics/kg1.png "knowledge-graph1")


Lastly, to upload the file that contains the semantic rules, go to the url: http://localhost:3061/upload_semantic_rules.
The following page will show up, where you can choose the file to upload (visual 3). 

Visual 3: Screen for uploading the .ttl file containing the semantic rules
![](pics/upload_semantic_rules1.png "upload_semantic")

After choosing the .ttl file, click on "Create Graph in Neo4j" and upon successful completion, the following message 
will appear:
- File successfully Uploaded

## Support
For any further assistance or clarifications, please contact eleni.voulgari@konnecta.io